window.addEventListener('load', (event) => {
  const confettiToggle = document.getElementById('confetti-toggle')
  const confettis = document.getElementById('confettis')
  confettiToggle.addEventListener('click', (event) => {
    confettis.hidden = !confettis.hidden
  })
})
