(function () {
  function redirectToNewGame () {
    const state = makeNewGameState()
    const url = stateToRelativeUrl(state)
    window.location = url
  }
  function stateFromCurrentUrl () {
    const url = new URL(document.URL)
    const stateString = url.searchParams.get('state')
    if (stateString === null) {
      redirectToNewGame()
    }
    const state = JSON.parse(stateString)
    return state
  }
  function stateToRelativeUrl (state) {
    const stateString = JSON.stringify(state)
    const urlParams = new URLSearchParams()
    urlParams.set('state', stateString)
    const url = new URL(document.URL)
    url.search = urlParams.toString()
    return url
  }
  function makeNewGameState () {
    return {
      version: 1,
      table: [1],
      hands: {},
      board: [
        [0, null],
        [null, null],
      ],
      pieces: [
        { color: 'white', shape: 'rook' },
        { color: 'black', shape: 'rook' },
      ],
    }
  }

  function join (state, playerJoining) {
    const newHands = {}
    for (const player in state.hands) {
      newHands[player] = state.hands[player]
    }
    newHands[playerJoining] = []
    return {
      version: 1,
      table: state.table,
      hands: newHands,
      board: state.board,
      pieces: state.pieces,
    }
  }

  function pickUp (state, pieceToPickUp, playerPickingUp) {
    const newTable = []
    for (const piece of state.table) {
      if (piece !== pieceToPickUp) {
        newTable.push(piece)
      }
    }
    const newHands = {}
    for (const player in state.hands) {
      const hand = state.hands[player]
      const newHand = []
      newHands[player] = newHand
      for (const piece of hand) {
        newHand.push(piece)
      }
      if (player === playerPickingUp) {
        newHand.push(pieceToPickUp)
      }
    }
    const newBoard = []
    for (const row of state.board) {
      const newRow = []
      newBoard.push(newRow)
      for (const square of row) {
        if (square === pieceToPickUp) {
          newRow.push(null)
        } else {
          newRow.push(square)
        }
      }
    }
    return {
      version: 1,
      table: newTable,
      hands: newHands,
      board: newBoard,
      pieces: state.pieces,
    }
  }

  const pieceTexts = {
    white_rook: '♖',
    black_rook: '♜',
  }

  function pieceHTML (state, pieceID, player) {
    const pieceData = state.pieces[pieceID]
    const pieceText = pieceTexts[`${pieceData.color}_${pieceData.shape}`]

    if (player !== null) {
      const pickedUpState = pickUp(state, pieceID, player)
      const pickedUpURL = stateToRelativeUrl(pickedUpState)

      const link = document.createElement('a')
      link.href = pickedUpURL
      link.innerText = pieceText
      return link
    } else {
      const span = document.createElement('span')
      span.innerText = pieceText
      return span
    }
  }

  function tableElement (state, player) {
    const table = document.createElement('span')
    table.classList.add('table')
    for (const pieceID of state.table) {
      table.appendChild(pieceHTML(state, pieceID, player))
    }
    return table
  }

  function gameElement (state, player) {
    const gameUI = document.createElement('div')
    gameUI.appendChild(tableElement(state, player))
    if (player === null) {
      const newID = Math.random().toString()
      const joinLink = document.createElement('a')
      joinLink.href = stateToRelativeUrl(join(state, newID))
      joinLink.innerText = 'join'
      gameUI.appendChild(joinLink)
    }
    const debug = document.createElement('pre')
    debug.innerText = JSON.stringify(state, null, 2)
    gameUI.appendChild(debug)
    return gameUI
  }

  const state = stateFromCurrentUrl()
  const player = null

  document.getElementById('game').appendChild(gameElement(state, player))
})()
