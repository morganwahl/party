// Defined in https://meet.jit.si/external_api.js
/* global JitsiMeetExternalAPI */

window.addEventListener('load', (event) => {
  const url = new URL(window.location)
  var room = url.searchParams.get('room')
  if (!room) {
    room = 'hallway'
    window.location = '?room=hallway'
  }
  const roomName = 'harperwahl-partyhouse-' + room
  const parent = document.getElementById('room')
  const api = new JitsiMeetExternalAPI('meet.jit.si', {
    roomName: roomName,
    parentNode: parent,
    configOverwrite: {
      enableNoisyMicDetection: false,
      requireDisplayName: false,
      hideLobbyButton: true,
      enableWelcomePage: false,
    },
    interfaceConfigOverwrite: {
      DEFAULT_REMOTE_DISPLAY_NAME: 'Fellow Party-goer',
      MOBILE_APP_PROMO: true,
      APP_NAME: 'Harper\u2010Wahl Party House',
      NATIVE_APPS_NAME: 'Harper\u2010Wahl Party House',
      HIDE_INVITE_MORE_HEADER: true,
    },
  })
})
