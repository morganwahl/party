// Defined in https://meet.jit.si/external_api.js
/* global JitsiMeetExternalAPI */

// https://developer.mozilla.org/en-US/docs/Web/API/Window/customElements
// /* global customElements */
// https://developer.mozilla.org/en-US/docs/Web/API/HTMLDivElement
// /* global HTMLDivElement */
// https://developer.mozilla.org/en-US/docs/Web/API/HTMLOptionElement/Option
/* global Option */
/* global FormData */

window.addEventListener('load', (event) => {
  'use strict'

  const SVG = 'http://www.w3.org/2000/svg'
  const url = new URL(window.location)
  var room = url.searchParams.get('room')
  if (!room) {
    room = 'living-room'
    window.location = '?room=living-room'
  }
  const roomElement = document.getElementById('room')
  roomElement.style.backgroundImage = `url("${room}-floor.jpg")`
  roomElement.classList.add(`room-${room}`)
  const roomName = 'andymetzgers-birthday-' + room
  const parent = document.getElementById('room-jitsi')
  const api = new JitsiMeetExternalAPI('meet.jit.si', {
    roomName: roomName,
    parentNode: parent,
    configOverwrite: {
      enableNoisyMicDetection: false,
      requireDisplayName: false,
      hideLobbyButton: true,
      enableWelcomePage: false,
    },
    interfaceConfigOverwrite: {
      DEFAULT_REMOTE_DISPLAY_NAME: 'Fellow Party\u2010Goer',
      MOBILE_APP_PROMO: true,
      APP_NAME: 'Andy\u2019s B-Day',
      NATIVE_APPS_NAME: 'Andy\u2019s B-Day',
      HIDE_INVITE_MORE_HEADER: true,
    },
  })
  window.addEventListener('beforeunload', function (event) {
    api.dispose()
  })

  class RoomSpace {
    constructor (jitsi) {
      this.jitsi = jitsi
      this.space = document.getElementById('room-space')
      this.table = document.getElementById('table')
      this.age = 0
      this.hands = {}
      this.participants = document.getElementById('participants')
      this.creatorForm = document.getElementById('creator')

      if (room === 'kitchen') {
        this.thingKinds = this.thingKindsFromText(`
          🍷 🥛 ☕ 🍵 🍶 🍾 🍸 🍺 🍹 🥃 🥤 🧃 🧉
          🍇 🍈 🍉 🍊 🍋 🍌 🍓 🍒 🥭 🍍 🍎 🍏 🍐 🍑 🥝
          🍆 🥑 🍅 🥥 🥕 🌽 🌶️ 🥒 🥬 🧄 🧅
          🍄
           🥜 🌰
           🦪 🐟 🦀
          🥓 🍖 🍗 🥩 🍳 🍿 🍠
          🥖 🥯 🥐 🥨
           🧀 🥟
            🧇  🍤
          🥗 🍔 🍟 🍕 🌭 🥪 🌮 🥙 🧆 🥘 🍲 🍱 🍛 🍜 🍝
           🍨 🍰
           🥄 🔪
          `)
      } else if (room === 'living-room') {
        this.thingKinds = this.thingKindsFromText(`
          🐚
          🎫 🎖️ 🏆 🏅 🖼️ 📰
          🎨 🎲 💶 💵
          `)
      } else if (room === 'back-yard') {
        this.thingKinds = this.thingKindsFromText(`
            🗡️  🌿 📢 🧭
          🏑 🏒 🎇 🗞️
          `)
      } else if (room === 'chess-lair') {
        this.thingKinds = this.thingKindsFromText(`
          ♔ ♕ ♖ ♗ ♘ ♙
          ♚ ♛ ♜ ♝ ♞ ♟︎
          `)
        for (const thingId in this.thingKinds) {
          const thing = this.thingKinds[thingId]
          thing.chesspiece = true
        }
      } else if (room === 'cozy-corner') {
        this.thingKinds = this.thingKindsFromText(`
          🍰 🍨
          🏓
          🥤 🧊 🥛
          🫖 ☕ 🍵 🍶 🍧
          🍷 🍺 🍸 🍹 🥃
          🍻 🍾 🥂 🫗 ⚗️
          🎁
          `)
      } else if (room === 'alley') {
        this.thingKinds = this.thingKindsFromText(`
        🍂
        🚬
        🔥
        🐀
        🥡
          `)
      } else if (room === 'closet') {
        this.thingKinds = this.thingKindsFromText(`
        🎲
        🧚 🧝 🧙 🧙‍♂️ 🧙‍♀️
        🏰
        👹 🐉 🦄 👻 👺 🦹 🧌 🧟
        ⚔️ ✨ 💀
          `)
      } else {
        this.thingKinds = this.thingKindsFromText(`
          🍙 🎲
        `)
      }
      for (const thingId in this.thingKinds) {
        const thing = this.thingKinds[thingId]
        if (thing.character === undefined) {
          thing.character = thingId
        }
      }

      this.initTable()
      this.initCreatorForm()
    }

    initTable () {
      this.table.style.backgroundImage = `url('${room}-table.jpg')`
      if (room === 'chess-lair') {
        const board = document.createElementNS(SVG, 'svg')
        board.classList.add('checkerboard')
        board.setAttribute('width', '100%')
        board.setAttribute('x', '0')
        board.setAttribute('y', '0')
        const ROWS = 8
        const COLUMNS = 8
        board.setAttribute('viewBox', `0 0 ${ROWS} ${COLUMNS}`)
        let color = 'white'
        for (let row = 0; row < ROWS; row++) {
          for (let col = 0; col < COLUMNS; col++) {
            const square = document.createElementNS(SVG, 'rect')
            square.classList.add('checker')
            square.classList.add(`checker-${color}`)
            square.setAttribute('x', col)
            square.setAttribute('y', row)
            square.setAttribute('width', '1')
            square.setAttribute('height', '1')
            square.setAttribute('fill', color)

            board.appendChild(square)
            if (color === 'white') {
              color = 'black'
            } else {
              color = 'white'
            }
          }
          if (color === 'white') {
            color = 'black'
          } else {
            color = 'white'
          }
        }
        this.table.appendChild(board)

        const pieceThingKindIds = [
          ['♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜'],
          ['♟︎', '♟︎', '♟︎', '♟︎', '♟︎', '♟︎', '♟︎', '♟︎'],
          [],
          [],
          [],
          [],
          ['♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙'],
          ['♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'],
        ]
        for (let row = 0; row < ROWS; row += 1) {
          for (let col = 0; col < COLUMNS; col += 1) {
            const pieceThingKindId = pieceThingKindIds[row][col]
            const x = col * (250 / COLUMNS)
            const y = 100 + row * (250 / ROWS)
            if (pieceThingKindId) {
              const piece = this.makeThingSVG(pieceThingKindId, x, y)
              // piece.setAttribute('font-size', '1')
              piece.addEventListener('click', this.pickUp.bind(this))
              this.table.appendChild(piece)
            }
          }
        }
      }
    }

    thingKindsFromText (text) {
      const textThings = text.trim()
      const thingKinds = {}
      for (const text of textThings.split(' ')) {
        if (!text.trim()) {
          continue
        }
        thingKinds[text.trim()] = {}
      }
      return thingKinds
    }

    initCreatorForm () {
      const thingKindSelector = this.creatorForm.querySelector('[name=thingKind]')
      for (const thingKindId in this.thingKinds) {
        const thingKind = this.thingKinds[thingKindId]
        const option = new Option(
          thingKind.character,
          thingKindId,
        )
        thingKindSelector.appendChild(option)
      }
    }

    participantsChanged (event) {
      const newParticipantsLIs = []
      for (const participantData of this.jitsi.getParticipantsInfo()) {
        const participantId = participantData.participantId
        const participantLI = document.createElement('li')
        for (const k in participantData) {
          const v = participantData[k]
          if (v === undefined) {
            continue
          }
          participantLI.dataset[k] = participantData[k]
        }
        let participantHTML = participantData.formattedDisplayName
        if (participantData.avatarURL) {
          participantHTML =
            '<img src="' + participantData.avatarURL + '" height=20>' +
            participantHTML
        }
        participantLI.innerHTML = participantHTML
        const icon = document.createElement('span')
        icon.classList.add('icon')
        participantLI.appendChild(icon)
        const holding = this.holding(participantId)
        if (holding) {
          const thingKind = this.thingKinds[holding]
          icon.innerText = thingKind.character + '🤏'
        } else {
          icon.innerText = '🖐️'
        }

        newParticipantsLIs.push(participantLI)
      }
      this.participants.replaceChildren(...newParticipantsLIs)
    }

    listen () {
      this.jitsi.addListener('avatarChanged', this.participantsChanged.bind(this))
      this.jitsi.addListener('displayNameChange', this.participantsChanged.bind(this))
      this.jitsi.addListener('emailChange', this.participantsChanged.bind(this))
      this.jitsi.addListener('participantJoined', this.participantsChanged.bind(this))
      this.jitsi.addListener('participantKickedOut', this.participantsChanged.bind(this))
      this.jitsi.addListener('participantLeft', this.participantsChanged.bind(this))

      this.jitsi.addListener('endpointTextMessageReceived', this.receiveMessage.bind(this))

      this.creatorForm.addEventListener('submit', this.create.bind(this))
      document.querySelector('#creator [type=submit]').disabled = false

      this.table.addEventListener('click', this.putDown.bind(this))
    }

    getMyParticipantId () {
      for (const pInfo of this.jitsi.getParticipantsInfo()) {
        /* <li data-display-name="nagrom" data-formatted-display-name="nagrom (me)" data-participant-id="4eac3126">nagrom (me)🖐️</li> */
        const me = 'me'
        if (pInfo.displayName) {
          if (pInfo.formattedDisplayName.endsWith(`(${me})`)) {
            return pInfo.participantId
          }
        } else {
          if (pInfo.formattedDisplayName === me) {
            return pInfo.participantId
          }
        }
      }
      console.warn("Can't find ID for current participant.")
    }

    hand (participantId) {
      const existingHand = this.hands.querySelector(`[data-participant-id="${participantId}"]`)
      if (existingHand) {
        return existingHand
      }
      const newHand = document.createElementNS(SVG, 'text')
      newHand.dataset.participantId = participantId
      this.hands.appendChild(newHand)
      return newHand
    }

    holding (participantId) {
      return this.hands[participantId]
    }

    hold (participantId, thingKindId) {
      this.hands[participantId] = thingKindId
      this.participantsChanged()
      this.sendState()
    }

    release (participantId) {
      this.hands[participantId] = ''
      this.participantsChanged()
      this.sendState()
    }

    create (event) {
      event.preventDefault()
      const participantId = this.getMyParticipantId()
      if (!participantId) {
        return
      }
      if (this.holding(participantId)) {
        return
      }
      const data = new FormData(event.target)
      const thingKindId = data.get('thingKind')
      this.age += 1
      this.hold(participantId, thingKindId)
    }

    putDown (event) {
      event.preventDefault()

      const participantId = this.getMyParticipantId()
      if (!participantId) {
        return
      }
      const thingKindId = this.holding(participantId)
      if (!thingKindId) {
        return
      }

      const thing = this.makeThingSVG(thingKindId, event.offsetX, event.offsetY)
      this.table.appendChild(thing)
      // Not needed because release() calls it too.
      // this.sendState()

      this.age += 1
      this.release(participantId)

      thing.addEventListener('click', this.pickUp.bind(this))
    }

    makeThingSVG (thingKindId, x, y) {
      const thingKind = this.thingKinds[thingKindId]
      const thing = document.createElementNS(SVG, 'text')
      thing.classList.add('thing')
      if (x !== undefined) {
        thing.setAttribute('x', x)
      }
      if (y !== undefined) {
        thing.setAttribute('y', y)
      }
      thing.textContent = thingKind.character
      thing.dataset.thingKindId = thingKindId
      if (thingKind.chesspiece) {
        thing.classList.add('chesspiece')
        thing.setAttribute('stroke', 'grey')
      }
      return thing
    }

    pickUp (event) {
      event.preventDefault()

      const participantId = this.getMyParticipantId()
      if (!participantId) {
        return
      }

      if (this.holding(participantId)) {
        return
      }

      const thing = event.target
      const thingKindId = thing.dataset.thingKindId
      thing.remove()
      this.age += 1
      this.hold(participantId, thingKindId)
      // not needed because hold() calls it too
      // this.sendState()

      // prevent the putDown handler triggering.
      event.stopPropagation()
    }

    sendState () {
      const state = {
        age: this.age,
        hands: this.hands,
        table: this.table.innerHTML,
      }
      this.broadcastState(state)
    }

    receiveMessage (event) {
      const message = event.data.eventData.text
      if (message === 'initState') {
        this.sendState()
      } else {
        this.receiveState(message)
      }
    }

    receiveState (data) {
      if (!data.startsWith('stateData')) {
        return
      }
      const state = JSON.parse(data.slice('stateData'.length))
      if (!state.age) {
        return
      }
      if (state.age <= this.age) {
        return
      }
      this.age = state.age
      this.hands = state.hands
      this.table.innerHTML = state.table
      // reattach listners
      for (const thing of document.querySelectorAll('#table .thing')) {
        thing.addEventListener('click', this.pickUp.bind(this))
      }
      this.participantsChanged()
    }

    broadcastState (data) {
      const json = 'stateData' + JSON.stringify(data)
      for (const participantData of this.jitsi.getParticipantsInfo()) {
        this.jitsi.executeCommand('sendEndpointTextMessage', participantData.participantId, json)
      }
    }
  }

  const space = new RoomSpace(api)
  space.listen()

  window.setInterval(function () {
    const participantsInfo = api.getParticipantsInfo()
    if (participantsInfo.length < 2) {
      return
    }
    const firstParticipantId = participantsInfo[0].participantId
    const myId = space.getMyParticipantId()
    if (myId === firstParticipantId) {
      space.sendState()
    }
  }, 1000)
})
